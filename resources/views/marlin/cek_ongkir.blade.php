<form method="post">
    {{ csrf_field() }}
    <p>Kota asal <strong>Jogja</strong></p>
    <div>
        <label>Silahkan Pilih Kota Tujuan</label>
        @if($kota)
        <select name="kota_tujuan">
            @foreach( $kota as $kota_ongkir )
            <option value='{{ $kota_ongkir['city_id']}}'>
                {{ $kota_ongkir['city_name'] }}
            </option >
            @endforeach
        </select>
        @else
        <label>Isi manual kota tujuan karena api sedang bermasalah</label>
        <input type="number" name="kota_tujuan" placeholder="silahkan masukan id kota tujuan">
        @endif
    </div>
    <br>
    <br>
    <div>
        <label>Pilih Kurir</label>
        <select name='kurir'>
            <option value='jne'>JNE</option>
            <option value='tiki'>TIKI</option>
            <option value='pos'>POS</option>
        </select>
    </div>
    <br>
    <div>
        <input type="number" name="berat" placeholder="masukan berat dalam gram">
    </div>
    <button type="submit">Cek ongkir</button>
    <br>
</form>
@if(isset($eror) && $eror)
<p> <strong>proses eror</strong>> </p>
@endif

@if(isset($service) && $service && isset($service['results'][0]))
<p>Service : {{ $service['results'][0]['name'] }}</p>

<p>kota asal : {{ $service['origin_details']['city_name'] }}</p>
<p>kota tujuan : {{ $service['destination_details']['city_name'] }}</p>
    @if(isset($service['results'][0]['costs'][0]))
        @foreach( $service['results'][0]['costs'] as $cost )
         <p>Harga : {{ $cost['cost'][0]['value'] }}</p>
         <p>Type : {{ $cost['service'] }}</p>
        @endforeach
     @else
        <p>cost not found</p>
    @endif

@else
<p>Silahkan lakukan proses</p>
@endif