<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RajaOngkirService;

class CheckOngkir extends Controller
{
    protected $rajaOngkirService;

    public function __construct(RajaOngkirService $rajaOngkirService)
    {
        $this->rajaOngkirService = $rajaOngkirService;
    }

    public function get()
    {
        $kota = $this->rajaOngkirService->getKota();
        return view('marlin.cek_ongkir', ['kota' => $kota]);
    }

    public function post(Request $request)
    {
        $berat    = ($request->input('berat')) ? $request->input('berat') : 100;
        $kota     = $this->rajaOngkirService->getKota();
        $cekHarga = $this->rajaOngkirService->cekHarga($request->input('kota_tujuan'),
            $berat, $request->input('kurir'));
        $eror     = ($cekHarga) ? false : true;
        return view('marlin.cek_ongkir',
            ['kota' => $kota, 'service' => $cekHarga, 'eror' => $eror]);
    }
}