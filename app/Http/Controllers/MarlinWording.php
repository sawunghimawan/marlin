<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MarlinWordingService;
class MarlinWording extends Controller
{
    protected $marlinWordingService;
    public function __construct(MarlinWordingService $marlinWordingService)
    {
        $this->marlinWordingService = $marlinWordingService;
    }
    public function get()
    {
        return view('marlin.wording',['marlin_wording'=>[]]);
    }

    public function post(Request $request)
    {
        $limit = ($request->input('limit') && $request->input('limit') > 0) ? $request->input('limit'): 1;
        $marlinWording = $this->marlinWordingService->divisbelWording($limit);
        return view('marlin.wording',['marlin_wording'=>$marlinWording]);
    }
}