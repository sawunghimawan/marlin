<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarlinWordingService
 *
 * @author helloworld
 */

namespace App\Services;

class MarlinWordingService
{

    public function divisbelWording(string $limit)
    {
        $marlinBooking = 0;
        $word = [];
        for ($x = 1; $x <= $limit; $x++) {
            if ($marlinBooking >= 5) {
                break;
            } else if ($x % 3 == 0 && $x % 5 == 0) {
                $marlinBooking++;
                $word[] =  "Marlin booking -".$x;
            } else if ($x % 3 == 0 && $marlinBooking >= 2) {
                $word[]= "booking -".$x;
            } else if ($x % 5 == 0 && $marlinBooking >= 2) {
                $word[] = "marlin -".$x;
            } else if ($x % 3 == 0) {
                $word[]= "marlin-".$x;
            } else if ($x % 5 == 0) {
                $word[] = "Booking-".$x;
            }
        }
        return $word;
    }
}