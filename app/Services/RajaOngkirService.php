<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

/**
 * Description of RajaOngkirService
 *
 * @author helloworld
 */
class RajaOngkirService
{
    protected $client;
    protected $key        = '2767b19fbe7320fbb9c1b60c696efb2c';
    protected $baseUrl    = 'https://api.rajaongkir.com/starter/';
    protected $kotaIdAsal = '501'; //id kota jogja rajaongkir

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getKota()
    {
        try {
            $result     = $this->client->request('GET', $this->baseUrl.'city',
                [
                'http_errors' => false,
                'headers' => [
                    'key' => $this->key
                ]
            ]);
            $statuscode = $result->getStatusCode();
            if ($statuscode == 200) {
                $dataJson  = $result->getBody();
                $dataArray = json_decode($dataJson, true);
                return $dataArray['rajaongkir']['results'];
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function cekHarga(string $idKotaTujuan, string $berat, string $kurir)
    {
        try {
            $result     = $this->client->request('POST', $this->baseUrl.'cost',
                [
                'http_errors' => false,
                'headers' => [
                    'content-type' => 'application/x-www-form-urlencoded',
                    'key' => $this->key
                ], 'form_params' => [
                    'origin' => $this->kotaIdAsal,
                    'destination' => $idKotaTujuan,
                    'weight' => $berat,
                    'courier' => $kurir
                ]
            ]);
            $statuscode = $result->getStatusCode();
            if ($statuscode == 200) {
                $dataJson  = $result->getBody();
                $dataArray = json_decode($dataJson, true);
                return $dataArray['rajaongkir'];
            } else {
                return false;
            }
        } catch (\Exception $e) {

        }
    }
}