<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
route::get('marlin-wording','MarlinWording@get');
route::post('marlin-wording','MarlinWording@post');
route::get('check-ongkir','CheckOngkir@get');
route::post('check-ongkir','CheckOngkir@post');